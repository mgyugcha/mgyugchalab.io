import Icon from '@mdi/react'

type LinkButtonProps = {
  title: string,
  href: string,
  icon: string,
  color?: string,
}

export default function ({
  icon, title, href, color = 'white',
}: LinkButtonProps) {
  return (
    <a
      href={href}
      rel="noopener noreferrer"
      target="_blank"
      style={{ margin: '0px 5px' }}
    >
      <Icon
        path={icon}
        title={title}
        size={2}
        color={color}
      />
    </a>
  )
}
