import React from 'react'
import LinkButton from 'components/LinkButton'
import { mdiGitlab, mdiFacebook, mdiEmail } from '@mdi/js'
import particlesConfig from '../assets/particles.json'

export default class extends React.Component {
  componentDidMount() {
    // eslint-disable-next-line global-require
    require('particles.js')
    const { particlesJS } = window as any
    particlesJS('particles-js', particlesConfig)
  }

  render() {
    return (
      <>
        <div className="mg-section">
          <div id="particles-js">
            <div className="container content">
              <h1>Michael Yugcha</h1>
              <h5>Software Developer</h5>
              <div className="buttons">
                <LinkButton
                  href="https://gitlab.com/mgyugcha"
                  title="Gitlab"
                  icon={mdiGitlab}
                />
                <LinkButton
                  href="https://www.facebook.com/mgyugcha"
                  icon={mdiFacebook}
                  title="Facebook"
                />
                <LinkButton
                  href="mailto:mgyugcha@gmail.com"
                  icon={mdiEmail}
                  title="Correo electrónico"
                />
              </div>
            </div>
          </div>
          {/* <div className="mg-subsection">
            <div className="container">
              <h1>Desarrollador</h1>
              <h3>
                Proyectos
              </h3>
              <div className="row">
                <div className="one-half column">
                  <h3>
                    UpConta
                  </h3>
                  <p>
                    Software de contabilidad
                  </p>
                </div>
                <div className="one-half column">Eleven</div>
              </div>
              <p>Yo</p>
            </div>
          </div> */}
        </div>
      </>
    )
  }
}
