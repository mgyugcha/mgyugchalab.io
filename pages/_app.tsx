/* eslint-disable */
import Head from 'next/head'
import '../assets/styles.scss'

// This default export is required in a new `pages/_app.js` file.
export default function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>@mgyugcha · Michael Yugcha</title>
        <meta name="theme-color" content="#0a0a0a" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;600&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/dhg/Skeleton/css/normalize.css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/dhg/Skeleton/css/skeleton.css" />
      </Head>
      <Component {...pageProps} />
    </>
  )
}
